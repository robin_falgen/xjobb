	</section>
	<!-- END OF CONTENT -->
	
	<footer>
		<div class="terms-links">
			<a href="#">&copy; Luftgitarrer.se 2017</a>
			<a href="#">Användarvillkor</a>
			<a href="#">Allmänna villkor</a>
			<a href="#">Sekretesspolicy</a>
			<a href="#">Frakt & Returer</a>
		</div>
		
		<div class="social-media">
			<a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
			<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
			<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
		</div>
		
		<div class="clear"></div>
	</footer>
	
	</body>
</html>